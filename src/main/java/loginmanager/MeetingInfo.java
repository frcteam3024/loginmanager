package loginmanager;

import static loginmanager.Constants.MeetingCsv.ACTUAL_END;
import static loginmanager.Constants.MeetingCsv.DATE;
import static loginmanager.Constants.MeetingCsv.DURATION;
import static loginmanager.Constants.MeetingCsv.DEFAULT_HEADER;
import static loginmanager.Constants.MeetingCsv.END_TIME;
import static loginmanager.Constants.MeetingCsv.HEADER_WIDTH;
import static loginmanager.Constants.MeetingCsv.REQUIRED_SUBTEAMS;
import static loginmanager.Constants.MeetingCsv.START_TIME;
import static loginmanager.Constants.MeetingCsv.STATUS;
import static loginmanager.Constants.MeetingStatus.DELETED;
import static loginmanager.Constants.MeetingStatus.IN_SESSION;
import static loginmanager.Constants.MeetingStatus.NOT_STARTED;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Set;

import loginmanager.Constants.MeetingStatus;
import loginmanager.Constants.Subteam;

public class MeetingInfo extends CsvFile {

  /** index of the row containing the current meeting info; last row of the file */
  private static int currentDateRow = getInstance().getNumRows() - 1;
  /** instance of MeetingInfo class to call CSVFile methods in a static context */
  private static MeetingInfo instance;

  /**
   * Initialize MeetingInfo as an instance of CsvFile with the name "meeting-info"
   */
  private MeetingInfo() {
    super("meeting-info", DEFAULT_HEADER);
  }

  /**
   * If meeting hasn't already been started, create a new entry with the current
   * date and start time
   */
  static {
    final LocalDate currentDate = LocalDate.now();
    LocalDate lastMeetingDate;
    try {
      lastMeetingDate = getDate();
    } catch (DateTimeParseException e) {
      // no previous meetings on record, invalid value for last meeting date
      lastMeetingDate = LocalDate.EPOCH;
    }
    if (!lastMeetingDate.equals(currentDate)) {
      getInstance().newRow();
      currentDateRow++;
      setStatus(NOT_STARTED);
      setDate(currentDate);
      setStartTime(LocalTime.now());
    }
  }

  /**
   * Get an instance of the MeetingInfo class to use when calling any non-static
   * methods of CSVFile superclass in a static context. Only one instance will
   * ever be created.
   * @return an instance of the MeetingInfo class
   */
  private static synchronized MeetingInfo getInstance() {
    if (instance == null) {
      instance = new MeetingInfo();
    }
    return instance;
  }
  
  /**
   * @return Date of the current meeting
   */
  public static LocalDate getDate() {
    return getInstance().getCellContentsDate(currentDateRow, DATE);
  }

  /**
   * Set date of the current meeting
   * @param date  date of current meeting
   */
  private static void setDate(final LocalDate date) {
    getInstance().setCellContents(currentDateRow, DATE, date);
  }

  /**
   * @return Status of the current meeting, one of NOT_STARTED, IN_SESSION,
   * OVERTIME, ENDED, or DELETED
   */
  public static MeetingStatus getStatus() {
    try {
      return MeetingStatus.valueOf(getInstance().getCellContentsStr(currentDateRow, STATUS));
    } catch (IndexOutOfBoundsException e) {
      // meeting info deleted with endSeason, currentDateRow is past end of file
      return DELETED;
    }
  }

  /**
   * Set status of the current meeting to one of NOT_STARTED, IN_SESSION,
   * OVERTIME, ENDED, or DELETED
   * @param status  current status of meeting
   */
  public static void setStatus(final MeetingStatus status) {
    getInstance().setCellContents(currentDateRow, STATUS, status.toString());
  }

  /**
   * @return Set containing all subteams required to attend current meeting
   */
  public static Set<Subteam> getRequiredSubteams() {
    return getInstance().getCellContentsSubteams(currentDateRow, REQUIRED_SUBTEAMS);
  }

  /**
   * Set which subteams are required to attend current meeting
   * @param requiredSubteams  subteams required for current meeting
   */
  private static void setRequiredSubteams(final Set<Subteam> requiredSubteams) {
    getInstance().setCellContents(currentDateRow, REQUIRED_SUBTEAMS, requiredSubteams);
  }

  /**
   * @return Time current meeting was started
   */
  public static LocalTime getStartTime() {
    return getInstance().getCellContentsTime(currentDateRow, START_TIME);
  }

  /**
   * Set time current meeting started
   * @param startTime  time current meeting started
   */
  private static void setStartTime(final LocalTime startTime) {
    getInstance().setCellContents(currentDateRow, START_TIME, startTime);
  }

  /**
   * @return Time current meeting will be over
   */
  public static LocalTime getEndTime() {
    return getInstance().getCellContentsTime(currentDateRow, END_TIME);
  }

  /**
   * Set time current meeting will be over
   * @param endTime  time current meeting will be over
   */
  private static void setEndTime(final LocalTime endTime) {
    getInstance().setCellContents(currentDateRow, END_TIME, endTime);
  }

  /**
   * Length of current meeting, difference between start time and end time
   * @return meeting duration
   */
  public static double getDuration() {
    return getInstance().getCellContentsDouble(currentDateRow, DURATION);
  }

  /**
   * Set the length of the current meeting from start to end
   */
  private static void setDuration() {
    final double duration = Time.hoursBetween(getStartTime(), getEndTime());
    getInstance().setCellContents(currentDateRow, DURATION, duration);
  }

  /**
   * Set time that meeting was actually ended: when `endMeeting` was run
   * @param actualEnd  time meeting ended
   */
  private static void setActualEnd(final LocalTime actualEnd) {
    getInstance().setCellContents(currentDateRow, ACTUAL_END, actualEnd);
  }

  /**
   * Create an entry for a new meeting with the following information
   * @param date  date of meeting
   * @param requiredSubteams  subteams required for meeting
   * @param startTime  time meeting started
   * @param endTime  time meeting will end
   */
  public static void newMeeting(
      final Set<Subteam> requiredSubteams,
      final LocalTime endTime) {
    setRequiredSubteams(requiredSubteams);
    setEndTime(endTime);
    setDuration();
    setStatus(IN_SESSION);
  }

  /**
   * Tasks to run when meeting is ended:
   * Set time meeting was ended and save a backup of the file
   */
  public static void endMeeting() {
    setActualEnd(LocalTime.now());
    getInstance().writeBackup();
  }

  /**
   * Remove all rows containing meeting data
   */
  public static void reset() {
    final int lastRow = getInstance().getNumRows() - 1;
    for (int row = lastRow; row >= HEADER_WIDTH; row--) {
      getInstance().removeRow(row);
    }
  }

  /**
   * Overwrite file contents with contents of backup
   * @return True if backup restored, false if not
   */
  public static boolean restore() {
    return getInstance().restoreBackup();
  }

}
