package loginmanager;

import static loginmanager.Constants.Attendance.MIN_HOURS_PERCENTAGE;
import static loginmanager.Constants.Time.TIME_FORMATTER;
import static loginmanager.TextFormat.green;
import static loginmanager.TextFormat.red;

import java.time.LocalTime;
import java.util.Set;

import loginmanager.Constants.Subteam;

public class Student {

  /**
   * Set properties for a student that just joined the team: name, id, subteams
   * @param name  new student name
   * @param id  new student id
   * @param subteams  new student subteams
   */
  public void initializeFirstTime(
      final String name,
      final int id,
      final Set<Subteam> subteams
  ) {
    reset();
    setName(name);
    setSubteams(subteams);
  }

  /**
   * @return Name from student info file
   */
  public String getName() {
    return StudentInfo.getName(this);
  }

  /**
   * Set name used in both login sheet and student info files
   * @param name  name of student
   */
  public void setName(final String name) {
    StudentInfo.setName(this, name);
    LoginInfo.setName(this, name);
  }

  /**
   * @return Student id
   */
  public int getId() {
    return StudentInfo.getId(this);
  }

  /**
   * Set id used in student info sheet
   * @param id  Student id
   */
  public void setId(final int id) {
    StudentInfo.setId(this, id);
  }

  /**
   * @return Subteams of student from student info file
   */
  public Set<Subteam> getSubteams() {
    return StudentInfo.getSubteams(this);
  }

  /**
   * Set subteams of student in student info file
   * @param subteams  student subteams
   */
  public void setSubteams(final Set<Subteam> subteams) {
    StudentInfo.setSubteams(this, subteams);
  }

  /**
   * @return True if student is logged in, false if not. 
   * Data from student info file
   */
  public boolean isLoggedIn() {
    return StudentInfo.isLoggedIn(this);
  }

  /**
   * Set whether or not student is logged in in student info file
   * @param loggedIn  if student is logged in
   */
  private void setLoggedIn(final boolean loggedIn) {
    StudentInfo.setLoggedIn(this, loggedIn);
  }

  /**
   * @return True if student attendance is above the minimum attendance
   * threshold, false if below
   */
  public boolean isPassingRequirement() {
    return (percentHoursAttended() >= MIN_HOURS_PERCENTAGE);
  }

  /**
   * Set whether or not student is passing atternace requirement
   * in student info file
   * @param passingRequirement  if student is passing attendance requirement
   */
  private void setPassingRequirement(final boolean passingRequirement) {
    StudentInfo.setPassingRequirement(this, passingRequirement);
  }

  /**
   * @return True if student has a full or partial excused absence, false if not.
   * Gets data by testing if there are entries for EXCUSED_START and EXCUSED_END.
   */
  public boolean isExcused() {
    LocalTime excusedStart = getExcusedFrom();
    LocalTime excusedEnd = getExcusedUntil();
    return excusedStart != null || excusedEnd != null;
  }

  /**
   * @return Time this student's excused absence starts
   */
  public LocalTime getExcusedFrom() {
    return StudentInfo.getExcusedFrom(this);
  }

  /**
   * Set time this student's excused absence starts
   * @param excusedStart  time excused absence starts
   */
  public void setExcusedFrom(final LocalTime excusedStart) {
    StudentInfo.setExcusedFrom(this, excusedStart);
  }

  /**
   * @return Time this student's excused absence ends
   */
  public LocalTime getExcusedUntil() {
    return StudentInfo.getExcusedUntil(this);
  }

  /**
   * Set time this student's excused absence ends
   * @param excusedEnd  time excused absence ends
   */
  public void setExcusedUntil(final LocalTime excusedEnd) {
    StudentInfo.setExcusedUntil(this, excusedEnd);
  }

	/**
	 * @return Time student logged in. Data from login sheet file
	 */
	public LocalTime getLoginTime() {
		return LoginInfo.getLoginTime(this);
	}

  /**
   * Set student's login time in login sheet file
   * @param loginTime  time student logged in
   */
  private void setLoginTime(final LocalTime loginTime) {
    LoginInfo.setLoginTime(this, loginTime);
  }

	/**
	 * @return Time student logged out. Data from login sheet file
	 */
	public LocalTime getLogoutTime() {
		return LoginInfo.getLogoutTime(this);
	}

  /**
   * Set student's logout time in login sheet file
   * @param logoutTime  time student logged out
   */
  private void setLogoutTime(final LocalTime logoutTime) {
    LoginInfo.setLogoutTime(this, logoutTime);
  }

  /**
   * @return Number of hours student has attended current meeting. 
   * Calculates from login until logout time, or if student hasn't logged out,
   * until current time
   */
  private double getMeetingHoursAttended() {
    if (!hasLoggedIn()) {
      return 0.0;
    }
    final LocalTime loginTime = LoginInfo.getLoginTime(this);
    if (!hasLoggedOut()) {
      return Time.hoursBetween(loginTime, LocalTime.now());
    }
    final LocalTime logoutTime = LoginInfo.getLogoutTime(this);
    return Time.hoursBetween(loginTime, logoutTime);
  }

  /**
   * @return Total number of hours student has attended on record. Data from
   * student info file
   */
  private double getHoursAttended() {
    return StudentInfo.getHoursAttended(this);
  }

  /**
   * Set total number of hours student has attended in student info file
   * @param hoursAttended  total number of meeting hours student has attended
   * this season
   */
  private void setHoursAttended(final double hoursAttended) {
    StudentInfo.setHoursAttended(this, hoursAttended);
  }

  /**
   * @return Total number of meeting hours possible for student to have
   * attended. Excludes meetings not required for their subteam or any excused
   * absences
   */
  private double getHoursRequired() {
    return StudentInfo.getHoursRequired(this);
  }

  /**
   * Set total number of hours possible for student to have attended in
   * student info file
   * @param hoursRequired  total number of hours this season required for
   * student to attend
   */
  private void setHoursRequired(final double hoursRequired) {
    StudentInfo.setHoursRequired(this, hoursRequired);
  }

  /**
   * Set percentage of required meeting hours student has attended
   * @param attendancePercent  percentage of required hours student has attended
   */
  private void setAttendancePercent(final double attendancePercent) {
    StudentInfo.setAttendancePercent(this, attendancePercent);
  }

  /**
   * Increases total hours attended by number of hours attended in current
   * meeting, and updates if student is passing attendance requirement
   */
  private void updateHoursAttended() {
    double totalHoursAttended = getHoursAttended();
    totalHoursAttended += getMeetingHoursAttended();
    setHoursAttended(totalHoursAttended);
  }

  /**
   * If meeting is required for student, increase total number of possible
   * attendance hours by meeting duration. Otherwise, increase by number of
   * hours student attended current meeting for full or partial excused
   * absences. Updates if student is passing attendance requirement
   */
  private void updateHoursRequired() {
    if (!Meeting.wasPreviouslyEnded()) {
      double hoursRequired = getHoursRequired();
      if (Meeting.requiredFor(this)) {
        hoursRequired += Meeting.getDuration();
      }
      if (isExcused()) {
        hoursRequired -= Time.hoursBetween(getExcusedFrom(), getExcusedUntil());
      }
      setHoursRequired(hoursRequired);
    }
  }

  /**
   * @return Percentage of required hours student has attended, or 100% if
   * student has no hours on record. This value accounts for unsaved hours
   * attended in the current meeting.
   * @apiNote If student has stayed overtime or attended
   * meetings not required for their subteam, this percentage can be above 100%.
   */
  public double percentHoursAttended() {
    double hoursAttended = getHoursAttended();
    double hoursRequired = getHoursRequired();
    if (!hasLoggedOut()) {
      hoursAttended += getMeetingHoursAttended();
      if (!Meeting.wasPreviouslyEnded()) {
        hoursRequired += Meeting.requiredHoursElapsed(this);
      }
    }
    if (hoursRequired == 0) {
      return 100.0;
    }
    return 100.0 * hoursAttended / hoursRequired;
  }

  /**
   * Update attended and required hours, attendance percent and passing
   * requirement fields of student info sheet to accurately reflect the current
   * status of the student
   */
  public void updateAttendance(final boolean previouslyLoggedOut) {
    updateHoursAttended();
    if (!previouslyLoggedOut) {
      updateHoursRequired();
    }
    setAttendancePercent(percentHoursAttended());
    setPassingRequirement(isPassingRequirement());
  }

  /**
   * @return If student has logged in for current meeting. Data from login sheet
   */
  public boolean hasLoggedIn() {
    return LoginInfo.getLoginTime(this) != null;
  }

  /**
   * @return If student has logged out from current meeting. Data from login sheet
   */
  public boolean hasLoggedOut() {
    return LoginInfo.getLogoutTime(this) != null;
  }

  /**
   * Set LOGGED_IN field in student info sheet to true, and set login time in 
   * login sheet to current time
   */
  public void logIn() {
    final LocalTime loginTime = LocalTime.now();
    setLoggedIn(true);
    setLoginTime(loginTime);
    System.out.printf(green("%s %s logged in%n"), 
        loginTime.format(TIME_FORMATTER), getName());
  }

  /**
   * set LOGGED_IN field in student info sheet to false, set logout time in
   * login sheet to current time, and update attendance stats
   */
  public void logOut() {
    if (!hasLoggedIn()) {
      System.out.printf(red("%s forgot to log out at their last meeting and auto logout failed%n"),
          getName());
      logIn();
      return;
    }
    // if student logged in and out and then back in again
    final boolean previouslyLoggedOut = hasLoggedOut();
    final LocalTime logoutTime = LocalTime.now();
    setLoggedIn(false);
    setLogoutTime(logoutTime);
    LoginInfo.setMeetingHoursAttended(this, getMeetingHoursAttended());
    updateAttendance(previouslyLoggedOut);
    System.out.printf(green("%s %s logged out%n"),
        logoutTime.format(TIME_FORMATTER), getName());
  }

  /**
   * Reset all student data
   */
   public void reset() {
    setLoggedIn(false);
    setHoursAttended(0.0);
    setHoursRequired(0.0);
    setAttendancePercent(100.0);
    setPassingRequirement(true);
    setExcusedFrom(null);
    setExcusedUntil(null);
  }

}
