package loginmanager;

import static loginmanager.Constants.StudentCsv.ATTENDANCE_PERCENT;
import static loginmanager.Constants.StudentCsv.DEFAULT_HEADER;
import static loginmanager.Constants.StudentCsv.EXCUSED_FROM;
import static loginmanager.Constants.StudentCsv.EXCUSED_UNTIL;
import static loginmanager.Constants.StudentCsv.HEADER_WIDTH;
import static loginmanager.Constants.StudentCsv.HOURS_ATTENDED;
import static loginmanager.Constants.StudentCsv.HOURS_REQUIRED;
import static loginmanager.Constants.StudentCsv.ID;
import static loginmanager.Constants.StudentCsv.LOGGED_IN;
import static loginmanager.Constants.StudentCsv.NAME;
import static loginmanager.Constants.StudentCsv.PASSING_REQUIREMENT;
import static loginmanager.Constants.StudentCsv.SUBTEAM;

import java.time.LocalTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import loginmanager.Constants.Subteam;

public class StudentInfo extends CsvFile {
  
  /** map linking student objects to their corresponding row numbers in the file  */
  private static final Map<Student, Integer> studentToRow = new LinkedHashMap<>();
  /** instance of StudentInfo class to call CSVFile methods in a static context */
  private static StudentInfo instance;

  /**
   * Initialize StudentInfo as an instance of CsvFile with the name "student-info"
   */
  private StudentInfo() {
    super("student-info", DEFAULT_HEADER);
  }

  /**
   * Get an instance of the StudentInfo class to use when calling any non-static
   * methods of CSVFile superclass in a static context. Only one instance will
   * ever be created.
   * @return an instance of the StudentInfo class
   */
  private static synchronized StudentInfo getInstance() {
    if (instance == null) {
      instance = new StudentInfo();
    }
    return instance;
  }

  /**
   * Scan each row of the file and parse nae and id to initialize a student
   * obect. Create a link in the map between student and corresponding row in
   * the student-info file
   */
  public static void parseAllStudents() {
    for (int row = HEADER_WIDTH; row < getInstance().getNumRows(); row ++) {
      final Student student = parseStudent(row);
      studentToRow.put(student, row);
    }
  }

  /**
   * Scan the given row and initialize a student object with the name and id
   * defined in the row
   * @param row  row index to scan
   * @return Student obect
   */
  private static Student parseStudent(final int row) {
    final String name = getInstance().getCellContentsStr(row, NAME);
    final int id = getInstance().getCellContentsInt(row, ID);
    return StudentManager.initializeStudent(name, id);
  }

  /**
   * Create a new speadsheet row containing the id of the new student and a map
   * link betweeen new student and their row.
   * @param student  student to add
   * @param name  name of student
   * @param id  id of student
   */
  public static void addNewStudent(
      final Student student,
      final String name,
      final int id
  ) {
    final int row = getInstance().getNextAvailableRow();
    studentToRow.put(student, row);
    getInstance().newRow();
    setName(student, name);
    setId(student, id);
  }

  /**
   * Remove the spreadsheet row containing student and remove the mapping
   * between student and their row
   * @param student  student to remove
   */
  public static void removeStudent(final Student student) {
    final int studentRow = studentToRow.get(student);
    final Student[] allStudents = studentToRow.keySet().toArray(new Student[0]);
    // move each student up one row
    for (int i = studentRow + 1 - HEADER_WIDTH; i < allStudents.length; i++) {
      studentToRow.replace(allStudents[i], i - 1 + HEADER_WIDTH);
    }
    getInstance().removeRow(studentRow);
    studentToRow.remove(student);
  }

  /**
   * @return Set containing all student objects
   */
  public static Set<Student> getAllStudents() {
    return studentToRow.keySet();
  }

  /**
   * @param student  student to get name of
   * @return Name of the student as listed in the file
   */
  public static String getName(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsStr(row, NAME);
  }

  /**
   * Set the name of student in the file to newName
   * @param student  student to set name of
   * @param newName  student's new name
   */
  public static void setName(
      final Student student,
      final String newName
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, NAME, newName);
  }

  /**
   * @param student  student to get id of
   * @return Id of the student as listed in the file
   */
  public static int getId(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsInt(row, ID);
  }

  /**
   * Set the id of student in the file to newId
   * @param student  student to set if of
   * @param id  student's new id
   */
  public static void setId(
      final Student student,
      final int id
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, ID, id);
  }

  /**
   * @param student  student to get subteams of
   * @return Set of subteams the student belongs to
   */
  public static Set<Subteam> getSubteams(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsSubteams(row, SUBTEAM);
  }

  /**
   * Set the subteams of student to newSubteams
   * @param student  student to set subteams of
   * @param newSubteams  student's new subteams
   */
  public static void setSubteams(
      final Student student,
      final Set<Subteam> newSubteams
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, SUBTEAM, newSubteams);
  }

  /**
   * @param student  student to check if logged in
   * @return True if student is logged in, false otherwise
   */
  public static boolean isLoggedIn(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsBool(row, LOGGED_IN);
  }

  /**
   * Set login status of student to true or false
   * @param student  student to set whether or not logged in
   * @param loggedIn  whether logged in or not
   */
  public static void setLoggedIn(
      final Student student,
      final boolean loggedIn
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, LOGGED_IN, loggedIn);
  }

  /**
   * @param student  student to get hours attended of
   * @return Total number of meeting hours student has attended this season
   */
  public static double getHoursAttended(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsDouble(row, HOURS_ATTENDED);
  }

  /**
   * Set total number of meeting hours student has attended this season
   * @param student  student to set hours attended of
   * @param hoursAttended  total number of meeting hours attended
   */
  public static void setHoursAttended(
      final Student student,
      final double hoursAttended
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, HOURS_ATTENDED, hoursAttended);
  }

  /**
   * @param student  student to set hours required of
   * @return Total number of meeting hours required for student to attend this
   * season
   */
  public static double getHoursRequired(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsDouble(row, HOURS_REQUIRED);
  }

  /**
   * Set total number of meeting hours required for student to attend
   * @param student  student to set required hours of
   * @param hoursRequired  total number of hours required for student this season
   */
  public static void setHoursRequired(
      final Student student,
      final double hoursRequired
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, HOURS_REQUIRED, hoursRequired);
  }

  /**
   * Set percentage of required meeting hours student has attended
   * @param student  student to set attendance percent of
   * @param attendancePercent  precentage of required hours student has attended
   */
  public static void setAttendancePercent(
      final Student student,
      final double attendancePercent
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, ATTENDANCE_PERCENT, attendancePercent);
  }

  /**
   * Set whether or not student is passing attendance requirement
   * @param student  student to check if passing requirement
   * @param passingRequirement  if student is passing attendance requirement
   */
  public static void setPassingRequirement(
      final Student student,
      final boolean passingRequirement
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, PASSING_REQUIREMENT, passingRequirement);
  }

  /**
   * @param student  student to get start of excused absence
   * @return Time this student's excused absence starts
   */
  public static LocalTime getExcusedFrom(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsTime(row, EXCUSED_FROM);
  }

  /**
   * Set time this student's excused absence starts
   * @param student  student to set start of excused absence for
   * @param excusedStart  time excused absence starts
   */
  public static void setExcusedFrom(
      final Student student,
      final LocalTime excusedStart
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, EXCUSED_FROM, excusedStart);
  }
  
  /**
   * @param student  student to get end of excused absence
   * @return Time this student's excused absence ends
   */
  public static LocalTime getExcusedUntil(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsTime(row, EXCUSED_UNTIL);
  }

  /**
   * Set time this student's excused absence ends
   * @param student  student to set end of excused absence for
   * @param excusedEnd  time excused absence ends
   */
  public static void setExcusedUntil(
      final Student student,
      final LocalTime excusedEnd
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, EXCUSED_UNTIL, excusedEnd);
  }
  
  /**
   * Tasks to run when meeting is ended:
   * Backup the file
   */
  public static void endMeeting() {
    getInstance().writeBackup();
  }

  /**
   * Reset all student login data, but keep data such as name, id, and subteams
   */
  public static void reset() {
    for (final Student student : getAllStudents()) {
      student.reset();
    }
  }

  /**
   * Overwrite file contents with contents of backup
   * @return True if backup restored, false if not
   */
  public static boolean restore() {
    return getInstance().restoreBackup();
  }

}
