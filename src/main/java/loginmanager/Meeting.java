package loginmanager;

import static loginmanager.Constants.MeetingStatus.ENDED;
import static loginmanager.Constants.Time.DATE_FORMATTER;
import static loginmanager.Constants.Time.DEFAULT_WEEKDAY_END_TIME;
import static loginmanager.Constants.Time.DEFAULT_WEEKEND_END_TIME;
import static loginmanager.Constants.Time.TIME_FORMATTER;
import static loginmanager.TextFormat.green;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

import loginmanager.Constants.MeetingStatus;
import loginmanager.Constants.Subteam;

public class Meeting {
  private Meeting() {}

  /** whether the current meeting was perviously ended or not */
  private static boolean previouslyEnded = false;

  /**
   * @return if `endMeeting` was run previously during the current meeting
   */
  public static boolean wasPreviouslyEnded() {
    return previouslyEnded;
  }

  /**
   * @return Time current meeting started
   */
  public static LocalTime getStartTime() {
    return MeetingInfo.getStartTime();
  }

  /**
   * @return Time current meeting will end
   */
  public static LocalTime getEndTime() {
    return MeetingInfo.getEndTime();
  }

  /**
   * @return Date of current meeting
   */
  public static LocalDate getDate() {
    return MeetingInfo.getDate();
  }

  /**
   * @return Set containing all subteams required to attend current meeting
   */
  public static Set<Subteam> getRequiredSubteams() {
    return MeetingInfo.getRequiredSubteams();
  }

  /**
   * Length of current meeting, difference between start time and end time
   * @return meeting duration
   */
  public static double getDuration() {
    return MeetingInfo.getDuration();
  }

  /**
   * @return If current time is after meeting end time
   */
  public static boolean isOver() {
    return LocalTime.now().isAfter(MeetingInfo.getEndTime()); 
  }

  /**
   * @return Status of the current meeting, one of NOT_STARTED, IN_SESSION,
   * OVERTIME, ENDED, or DELETED
   */
  public static MeetingStatus getStatus() {
    return MeetingInfo.getStatus();
  }

  /**
   * Set status of the current meeting to one of NOT_STARTED, IN_SESSION,
   * OVERTIME, ENDED, or DELETED
   * @param status  current status of meeting
   */
  private static void setStatus(final MeetingStatus status) {
    MeetingInfo.setStatus(status);
  }

  /**
   * Set meeting status to OVERTIME
   */
  public static void toOvertime() {
    setStatus(MeetingStatus.OVERTIME);
  }

  /**
   * @return default end time depending on if the current meeting date is a 
   * weekday or weekend
   */
  public static LocalTime defaultEndTime() {
    if (Time.isWeekend(MeetingInfo.getDate())) {
      return DEFAULT_WEEKEND_END_TIME;
    } else {
      return DEFAULT_WEEKDAY_END_TIME;
    }
  }

  /**
   * Initialize a new meeting for current date required for the subteams
   * specified in requiredSubteams and set to end at endTime
   * @param endTime  when current meeting will end
   * @param requiredSubteams  subteams required for current meeting
   */
  public static void newMeeting(
      final LocalTime endTime,
      final Set<Subteam> requiredSubteams
  ) {
    final LocalDate date = getDate();
    MeetingInfo.newMeeting(requiredSubteams, endTime);
    LoginInfo.newMeeting(date);
    System.out.printf(green("Starting new meeting on %s at %s%n%n"),
        date.format(DATE_FORMATTER), getStartTime().format(TIME_FORMATTER));
  }

  /**
   * Resume meeting after an interruption such as ^C or pi undervoltage reboot
   */
  public static void resume() {
    LoginInfo.resumeMeeting();
    final LocalDate date = getDate();
    System.out.printf(green("Resuming meeting on %s at %s%n%n"),
        date.format(DATE_FORMATTER), LocalTime.now().format(TIME_FORMATTER));
  }

  /**
   * Resume meeting after endMeeting was run
   */
  public static void resumeEndedMeeting() {
    previouslyEnded = true;
    if (isOver()) {
      setStatus(MeetingStatus.OVERTIME);
    } else {
      setStatus(MeetingStatus.IN_SESSION);
    }
    resume();
  }

  /**
   * Meetings are required for students that aren't excused and that are in a
   * subteam required by the meeting 
   * @param student  student to check if current meeting is required for
   * @return If meeting is required for student
   */
  public static boolean requiredFor(final Student student) {
    final Set<Subteam> overlap = student.getSubteams();
    // remove all values not in required subteams
    overlap.retainAll(getRequiredSubteams());
    return !overlap.isEmpty();
  }

  /**
   * @return number of required hours for the given student since the beginning
   * of the meeting. Only time before the meeting is over and that the student
   * isn't excused during is counted
   */
  public static double requiredHoursElapsed(final Student student) {
    if (!Meeting.requiredFor(student)) {
      return 0.0;
    }
    final double meetingHoursElapsed = Time.hoursElapsedBetween(
        getStartTime(), getEndTime());
    final double excusedHoursElapsed;
    if (!student.isExcused()) {
      excusedHoursElapsed = 0.0;
    } else {
      excusedHoursElapsed = Time.hoursElapsedBetween(
        student.getExcusedFrom(), student.getExcusedUntil());
    }
    return meetingHoursElapsed - excusedHoursElapsed;
  }

  /**
   * Tasks to run when meeting is ended:
   * set status to ENDED, 
   * logout all students still logged in, 
   * update attendance stats for all students that didn't log attend
   * various tasks each spreadsheet needs to run
   */
  public static void end() {
    for (final Student student : StudentManager.getAllStudents()) {
      if (student.isLoggedIn()) {
        student.logOut();
      }
      if (!student.hasLoggedIn() && !previouslyEnded) {
        student.updateAttendance(false);
      }
    }
    MeetingInfo.endMeeting();
    LoginInfo.endMeeting();
    StudentInfo.endMeeting();
    setStatus(ENDED);
    final String endTime = TIME_FORMATTER.format(getEndTime());
    final String meetingDate = DATE_FORMATTER.format(getDate());
    System.out.printf(green("%s %s Meeting ended%n"), endTime, meetingDate);
  }

}
