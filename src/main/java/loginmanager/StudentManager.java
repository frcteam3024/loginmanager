package loginmanager;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import loginmanager.Constants.Subteam;

public class StudentManager {
  private StudentManager() {}

  /** map containing a linkage between names and their corresponding students */
  private static final Map<String, Student> nameToStudent = new HashMap<>();
  /** map containing a linkage between ids and their corresponding students */
  private static final Map<Integer, Student> idToStudent = new HashMap<>();

  static {
    StudentInfo.parseAllStudents();
  }
 
  /**
   * @return Collection of all student objects
   */
  public static Collection<Student> getAllStudents() {
    return idToStudent.values();
  }

  /**
   * Change the name associated with a student in all methods in this class
   * @param oldName  old name of student
   * @param newName  name to replace with
   */
  public static void changeName(final String oldName, final String newName) {
    Student student = nameToStudent.get(oldName);
    nameToStudent.remove(oldName);
    nameToStudent.put(newName, student);
  }

  /**
   * Change the id associated with a student in all methods in this class
   * @param oldId  old id of student
   * @param newId  id to replace with
   */
  public static void changeId(final int oldId, final int newId) {
    Student student = idToStudent.get(oldId);
    idToStudent.remove(oldId);
    idToStudent.put(newId, student);
  }

  /**
   * @return List of students sorted alphabetically by name
   */
  public static List<Student> getAllStudentsSorted() {
    final List<Student> allStudents = Arrays.asList(getAllStudents().toArray(new Student[0]));
    // sort with alphabetic comparison by name
    allStudents.sort((student1, student2) -> student1.getName()
        .compareTo(student2.getName()));  
    return allStudents;
  }

  /**
   * Initialize a student object with the given name and id and add it to the
   * name and id maps
   * @param name  student name
   * @param id  student id
   * @return the student object
   */
  public static Student initializeStudent(
      final String name,
      final int id
  ) {
    final Student student = new Student();
    nameToStudent.put(name, student);
    idToStudent.put(id, student);
    return student;
  }

  /**
   * Initialize a new student with the given attributes and add it to LoginInfo
   * and StudentInfo
   * @param name  student name
   * @param id  student id
   * @param subteams  student subteams
   * @return the student object
   */
  public static Student initializeNewStudent(
      final String name,
      final int id,
      final Set<Subteam> subteams
  ) {
    final Student student = initializeStudent(name, id);
    StudentInfo.addNewStudent(student, name, id);
    LoginInfo.addNewStudent(student);
    student.initializeFirstTime(name, id, subteams);
    return student;
  }

  /**
   * Remove given student from LoginInfo and StudentInfo spreadsheets.
   * This action cannot be undone.
   * @param student  student to remove
   */
  public static void removeStudent(final Student student) {
    nameToStudent.remove(student.getName());
    idToStudent.remove(student.getId());
    LoginInfo.removeStudent(student);
    StudentInfo.removeStudent(student);
  }

  /**
   * @param id  id to search for
   * @return if a student in the system has the given id
   */
  public static boolean containsId(final int id) {
    return idToStudent.containsKey(id);
  }

  /**
   * @param name  name to search for
   * @return if a student in the system has the given name
   */
  public static boolean containsName(final String name) {
    return nameToStudent.containsKey(name);
  }

  /**
   * @param id  desired id to search for
   * @return student object with a matching id, or null if no matches exist
   */
  public static Student getStudent(final int id) {
    return idToStudent.get(id);
  }

  /**
   * @param name  desired name to search for
   * @return student object with a matching name, or null if no matches exist
   */
  public static Student getStudent(final String name) {
    return nameToStudent.get(name);
  }
}
