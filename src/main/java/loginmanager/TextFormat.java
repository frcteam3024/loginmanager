package loginmanager;

import static loginmanager.Constants.Format.BLUE;
import static loginmanager.Constants.Format.BOLD;
import static loginmanager.Constants.Format.CYAN;
import static loginmanager.Constants.Format.GREEN;
import static loginmanager.Constants.Format.MAGENTA;
import static loginmanager.Constants.Format.RED;
import static loginmanager.Constants.Format.RESET;
import static loginmanager.Constants.Format.YELLOW;

public class TextFormat {
  private TextFormat() {}

  /**
   * @param msg  text to format
   * @param color  color to format text to
   * @return msg with color formatting applied
   * @apiNote For more information look at link in Constants.Format
   */
  private static String color(final String msg, final String color) {
    return color + BOLD + msg + RESET;
  }

  /**
   * @param msg  text to format
   * @return msg with red formatting applied
   */
  public static String red(final String msg) {
    return color(msg, RED);
  }

  /**
   * @param msg  text to format
   * @return msg with green formatting applied
   */
  public static String green(final String msg) {
    return color(msg, GREEN);
  }

  /**
   * @param msg  text to format
   * @return msg with yellow formatting applied
   */
  public static String yellow(final String msg) {
    return color(msg, YELLOW);
  }

  /**
   * @param msg  text to format
   * @return msg with blue formatting applied
   */
  public static String blue(final String msg) {
    return color(msg, BLUE);
  }

  /**
   * @param msg  text to format
   * @return msg with magenta formatting applied
   */
  public static String magenta(final String msg) {
    return color(msg, MAGENTA);
  }

  /**
   * @param msg  text to format
   * @return msg with cyan formatting applied
   */
  public static String cyan(final String msg) {
    return color(msg, CYAN);
  }

  /**
   * Get a string of padding spaces to pad string to the desired length
   * @param str  string to pad
   * @param size  desired final length of string
   * @return string of spaces with length the difference between string length
   * and desired length
   */
  private static String getPadding(final String str, final int size) {
    final int padSpaces = size - str.length();
    if (padSpaces <= 0) {
      return "";
    }
    return new String(new char[padSpaces]).replace('\0', ' ');
  }

  /**
   * Pad string on the right with enough spaces to make it the desired length
   * @param str  string to format
   * @param size  final length of the string
   * @return padded string aligned right
   */
  public static String alignRight(final String str, final int size) {
    final String padding = getPadding(str, size);
    return padding + str;
  }

  /**
   * Pad string on the left with enough spaces to make it the desired length
   * @param str  string to format
   * @param size  final length of the string
   * @return padded string aligned left
   */
  public static String alignLeft(final String str, final int size) {
    final String padding = getPadding(str, size);
    return str + padding;
  }
  
  /**
   * Center string with padding spaces on the left and right to make it the
   * desired length
   * @param str  string to format
   * @param size  final length of the string
   * @return padded string aligned to the center
   */
  public static String alignCenter(String str, final int size) {
    final int strLen = str.length();
    final int padSpaces = size - strLen;
    str = alignLeft(str, strLen + padSpaces / 2);
    str = alignRight(str, size);
    return str;
  }

}
