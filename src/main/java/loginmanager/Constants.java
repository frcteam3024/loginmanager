package loginmanager;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Constants {
  private Constants() {}

  public static class Attendance {
    private Attendance() {}
    
    /** percentage of required hours students must attend */
    public static final double MIN_HOURS_PERCENTAGE = 80;
  }

  public static class Csv {
    private Csv() {}
    
    /** path to folder containing spreadsheet files */
    private static final Path CSV_DIR_PATH = Paths.get("../resources");
    /** spreadsheet folder as a file object */
    public static final File CSV_DIR = CSV_DIR_PATH.toFile();
    /** suffix to append to all spreadsheet file names */
    public static final String BACKUP_SUFFIX = "-backup";
    /** file extension for spreadsheet files */
    public static final String CSV_EXTENSION = ".csv";
    /** character used to separate entries in spreadsheet */
    public static final String DELIMETER = ";";
    /** index of row containing header data (first row) */
    public static final int HEADER_ROW   = 0;
    /** number of rows header is (only one row) */
    public static final int HEADER_WIDTH = 1;
    /** number of decimal places to round numbers to in spreadsheet */
    public static final int NUM_DECIMAL_PLACES = 3;
  }

  public static class StudentCsv extends Csv {
    private StudentCsv() {}

    public static final List<String> DEFAULT_HEADER = Collections.unmodifiableList(
        Arrays.asList("NAME", "ID", "SUBTEAM", "LOGGED_IN", "HOURS_ATTENDED",
        "HOURS_REQUIRED", "ATTENDANCE_PERCENT", "PASSING_REQUIREMENT",
        "EXCUSED_START", "EXCUSED_END"));
    // column indicies of fields in student info spreadsheet
    public static final int NAME                = 0;
    public static final int ID                  = 1;
    public static final int SUBTEAM             = 2;
    public static final int LOGGED_IN           = 3;
    public static final int HOURS_ATTENDED      = 4;
    public static final int HOURS_REQUIRED      = 5;
    public static final int ATTENDANCE_PERCENT  = 6;
    public static final int PASSING_REQUIREMENT = 7;
    public static final int EXCUSED_FROM        = 8;
    public static final int EXCUSED_UNTIL       = 9;
  }
  
  public static class LoginCsv extends Csv {
    private LoginCsv() {}

    public static final List<String> DEFAULT_HEADER = Collections.unmodifiableList(
        Arrays.asList("NAME"));
    // column indicies of fields in login info spreadsheet
    public static final int NAME                  = 0;
    public static final int DATE_START_INDEX      = 1;

    /** each meeting date contains 3 coluns of data */
    public static final int DATE_BLOCK_WIDTH      = 3;
    // indicies of columns with a meeting date block
    public static final int LOGIN_COL_OFFSET      = 0;
    public static final int LOGOUT_COL_OFFSET     = 1;
    public static final int HOURS_ATTENDED_OFFSET = 2;
  }

  public static class MeetingCsv extends Csv {
    private MeetingCsv() {}

    public static final List<String> DEFAULT_HEADER = Collections.unmodifiableList(
        Arrays.asList("DATE", "STATUS", "REQUIRED_SUBTEAMS", "START_TIME",
        "END_TIME", "DURATION", "ACTUAL_END"));
    // column indicies of fields in meeting info spreadsheet
    public static final int DATE              = 0;
    public static final int STATUS            = 1;
    public static final int REQUIRED_SUBTEAMS = 2;
    public static final int START_TIME        = 3;
    public static final int END_TIME          = 4;
    public static final int DURATION          = 5;
    public static final int ACTUAL_END        = 6;
  }

  public static class Time {
    private Time() {}

    // basic unit conversions
    public static final double MINS_PER_HOUR = 60.0;
    public static final double HOURS_PER_DAY = 24.0;

    /* how to format times and dates, documentation here:
       https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html 
       look under 'Patterns for Formatting and Parsing' */
    // QUINN MY BROTHER IN CHRIST 24 HOUR TIME FIXED EVERYTHING
    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MMM/yyyy");
    public static final LocalTime DEFAULT_WEEKDAY_END_TIME = LocalTime.parse("18:00", TIME_FORMATTER);
    public static final LocalTime DEFAULT_WEEKEND_END_TIME = LocalTime.parse("15:00", TIME_FORMATTER);
  }

  /** all the subteams */
  public enum Subteam {
    ADMIN,
    CAD,
    ELECTRICAL,
    MECHANICAL,
    PROGRAMMING
  }

  /** all possible states for a meeting to be in */
  public enum MeetingStatus {
    NOT_STARTED,
    IN_SESSION,
    OVERTIME,
    ENDED,
    DELETED
  }

  public static class Format {
    private Format() {}

    /* ANSI escape sequences explained here:
       https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences */

    public static final String RESET   = "\u001B[0m";
    public static final String BOLD    = "\u001B[1m";
    public static final String RED     = "\u001B[31m";
    public static final String GREEN   = "\u001B[32m";
    public static final String YELLOW  = "\u001B[33m";
    public static final String BLUE    = "\u001B[34m";
    public static final String MAGENTA = "\u001B[35m";
    public static final String CYAN    = "\u001B[36m";
    public static final String WHITE   = "\u001B[37m";
  }

  public static class IO {
    private IO() {}

    /** color escape sequences add 13 to a string's length */
    public static final int COLOR_LENGTH_OFFSET = 13;

    // column indicies of fields when printing status
    public static final int NAME       = 0;
    public static final int ID         = 1;
    public static final int LOGIN      = 2;
    public static final int LOGOUT     = 3;
    public static final int ATTENDANCE = 4;

    // minimum field widths for dynamically sized fields
    public static final int MIN_NAME_LENGTH   =  4;
    public static final int MIN_ID_LENGTH     =  2;
    // set fixed width for each of the following fields when printing status
    public static final int LOGIN_LENGTH      =  7;
    public static final int LOGOUT_LENGTH     =  7;
    public static final int ATTENDANCE_LENGTH = 10;
  }

}
