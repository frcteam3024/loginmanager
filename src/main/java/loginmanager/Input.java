package loginmanager;

import static loginmanager.Constants.IO.ATTENDANCE;
import static loginmanager.Constants.IO.ATTENDANCE_LENGTH;
import static loginmanager.Constants.IO.COLOR_LENGTH_OFFSET;
import static loginmanager.Constants.IO.ID;
import static loginmanager.Constants.IO.LOGIN;
import static loginmanager.Constants.IO.LOGIN_LENGTH;
import static loginmanager.Constants.IO.LOGOUT;
import static loginmanager.Constants.IO.LOGOUT_LENGTH;
import static loginmanager.Constants.IO.NAME;
import static loginmanager.Constants.Time.TIME_FORMATTER;
import static loginmanager.TextFormat.alignCenter;
import static loginmanager.TextFormat.alignRight;
import static loginmanager.TextFormat.blue;
import static loginmanager.TextFormat.cyan;
import static loginmanager.TextFormat.green;
import static loginmanager.TextFormat.red;
import static loginmanager.TextFormat.yellow;
import static loginmanager.Constants.IO.*;

import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import loginmanager.Constants.Subteam;

public class Input {
  private Input() {}

  private static Scanner scanner = new Scanner(System.in);

  /**
   * Prompts user with msg for input
   * @param msg  Message to prompt the user with
   * @return User inputted String
   */
  private static String getInput(final String msg) {
    System.out.print(msg);
    return scanner.nextLine().trim();
  }

  /**
   * Prompts user with msg for a student name. Repeats until user input is not
   * empty, "null", or the name of another existing student.
   * @param msg  Message to prompt the user with
   * @return Valid inputted name
   */
  private static String getNewName(final String msg) {
    String name;
    boolean validName;
    do {
      name = getInput(msg);
      validName = true;
      if (name.isEmpty()) {
        System.out.println("Name cannot be empty, try again");
        validName = false;
      } else if (name.equals("null")) {
        System.out.println("Name cannot be \"null\", try again");
        validName = false;
      } else if (StudentManager.containsName(name)) {
        System.out.printf(red("There is already a student with name %s, try again%n"), name);
        validName = false;
      }
    } while (!validName);
    return name;
  }

  /**
   * Prompts user with msg for a student ID.
   * Repeats until a positive integer is entered
   * @param msg  Message to prompt the user with
   * @return User inputted ID
   */
  private static int getIdInt(final String msg) {
    String input;
    int id = -1;  // only initializing so code compiles, value is meaningless
    boolean validId;
    do {
      input = getInput(msg);
      validId = true;
      if (!isInteger(input)) {
        System.out.println(red("ID must only contain characters 0-9, try again"));
        validId = false;
      } else {
        id = Integer.parseInt(input);
        if (id <= 0) {
          System.out.println(red("ID must be a positive integer, try again"));
          validId = false;
        }
      }
    } while (!validId);
    return id;
  }

  /**
   * Get an id that doesn't belong to any student in the system
   * @param msg  Message to prompt the user with
   * @return Unused id
   */
  private static int getNewId(final String msg) {
    int id;
    boolean validId;
    do {
      validId = true;
      id = getIdInt(msg);
      if (StudentManager.containsId(id)) {
        System.out.printf(red("There is already a student with id %d, try again%n"), id);
        validId = false;
      }
    } while (!validId);
    return id;
  }

  /**
   * Get an id that belongs to a student in the system
   * @param msg  Message to prompt the user with
   * @return Used id
   */
  private static int getUsedId(final String msg) {
    int id;
    boolean validId;
    do {
      validId = true;
      id = getIdInt(msg);
      if (!StudentManager.containsId(id)) {
        System.out.printf(red("No student with id %d, try again%n"), id);
        validId = false;
      }
    } while (!validId);
    return id;
  }

  /**
   * Lists subteams and corresponding index number. Prompts user with msg. User
   * enters a combination of numbers, defaults to none if no input is entered
   * @param msg  Message to prompt the user with
   * @return Set of subteams based off of user input
   */
  private static Set<Subteam> getSubteamsDefaultNone(final String msg) {
    final Subteam[] allSubteams = Subteam.values();
    final int maxIndex = allSubteams.length;
    for (int index = 0; index < maxIndex; index++) {
      System.out.printf("%d - %s%n", index, allSubteams[index]);
    }
    final String input = getInput(msg);
    final Set<Subteam> selectedSubteams = new HashSet<>();
    for (final char c : input.toCharArray()) {
      // do math with ascii value for 0 to avoid converting to int and back
      if ('0' <= c && c <= '0' + maxIndex) {
        int index = c - '0';
        selectedSubteams.add(allSubteams[index]);
      } else {
        System.out.println(red("Invalid option \'" + c + "\'"));
      }
    }
    return selectedSubteams;
  }

  /**
   * Get a set containing at least one subteam
   * @param msg  Message to prompt the user with
   * @return Set of one or more subteams
   */
  private static Set<Subteam> getOneOrMoreSubteams(final String msg) {
    Set<Subteam> subteams;
    do {
      subteams = getSubteamsDefaultNone(msg);
      if (subteams.isEmpty()) {
        System.out.println(red("You must enter at least one subteam, try again"));
      }
    } while (subteams.isEmpty());
    return subteams;
  }

  /**
   * Ask user for a time, default to defaultTime if nothing is entered
   * @param msg  Message to prompt the user with
   * @param defaultTime  time to return if input is empty
   * @return Time inputted by user, or defaultTime if input empty
   */
  private static LocalTime getTime(
      final String msg,
      final LocalTime defaultTime
  ) {
    boolean invalidFormat;
    LocalTime time = defaultTime;
    do {
      String timeStr = getInput(msg + "[" + TIME_FORMATTER.format(defaultTime) + "] ");
      try {
        if (!timeStr.isEmpty()) {
          time = LocalTime.parse(timeStr, TIME_FORMATTER);
        }
        invalidFormat = false;
      } catch (DateTimeParseException e) {
        System.out.println("Invalid time format. Use h:mm(AM/PM) instead");
        invalidFormat = true;
      }
    } while (invalidFormat);
    return time;
  }

  /**
   * Tests whether input is a valid integer
   * @param input integer to test
   * @return true if input is a valid integer, false otherwise
   */
  private static boolean isInteger(final String input) {
    try {
      Integer.parseInt(input);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  /**
   * Prompt the user with msg and ask for a yes/no choice.
   * Only returns true if user input begins with a 'y' or 'Y'.
   * For use when actions cannot be undone.
   * @param msg  Message to prompt the user with
   * @return If user responded in the affirmative
   */
  private static boolean getConfirmationDefaultNo(final String msg) {
    final String input = getInput(yellow(msg + " [y/N] "));
    return input.toLowerCase().startsWith("y");
  }

  /**
   * Prompt the user with msg and ask for a yes/no choice.
   * Returns true if user input begins with a 'y' or 'Y' or if input is empty.
   * For use when actions can be undone.
   * @param msg  Message to prompt the user with
   * @return If user responded in the affirmative
   */
  private static boolean getConfirmationDefaultYes(final String msg) {
    final String input = getInput(yellow(msg + " [Y/n] "));
    return input.toLowerCase().startsWith("y") || input.isEmpty();
  }

  /**
   * Execute command corresponding to user input
   * @param command  command to execute
   */
  private static void commandAction(final String command) {
    switch (command) {
      case "help":
        printHelp();
        break;
      case "clear":
	System.out.print("\033\143");
	welcome();
	break;
      case "changeId":
        changeStudentId();
        break;
      case "changeName":
        changeStudentName();
        break;
      case "changeSubteams":
        changeStudentSubteams();
        break;
      case "endMeeting":
        Meeting.end();
        break;
      case "endSeason":
        endSeason();
        break;
      case "excuse":
        excuseStudent();
        break;
      case "newStudent":
        addNewStudent();
        break;
      case "removeStudent":
        removeStudent();
        break;
      case "restoreBackups":
        restoreBackups();
        break;
      case "status":
        printStatus();
        break;
      default:
        System.out.printf(red("%nIllegal command \"%s\""), command);
        printHelp();
    }
  }

  /**
   * Display all available commands with a short description for each
   */
  private static void printHelp() {
    System.out.println("""
        
        Available commands:
        help           - print this help message
	clear          - clear console, show logo
        changeId       - change student id
        changeName     - change student name
        changeSubteams - change student subteams
        endMeeting     - RUN AT THE END OF EVERY MEETING
        endSeason      - reset spreadsheets for next season
        excuse         - excuse student for time interval
        newStudent     - add new student
        removeStudent  - remove student from spreadsheet
        restoreBackups - restore data from backup files
        status         - list which students are logged in
      """);
  }

  /**
   * Get id of student to excuse and times student is excused from and until,
   * then set those times in student info sheet
   */
  private static void excuseStudent() {
    final int id = getUsedId("Enter id of student to excuse: ");
    final LocalTime meetingStart = Meeting.getStartTime();
    final LocalTime meetingEnd = Meeting.getEndTime();
    LocalTime excusedFrom;
    boolean validTime;
    do {
      validTime = true;
      excusedFrom = getTime("Excused from: ", meetingStart);
      if (excusedFrom.isBefore(meetingStart)) {
        System.out.println(red("Excuse start must not be before meeting start, try again"));
        validTime = false;
      } else if (excusedFrom.isAfter(meetingEnd)) {
        System.out.println(red("Excuse start must not be after meeting end, try again"));
        validTime = false;
      }
    } while (!validTime);
    LocalTime excusedUntil;
    do {
      validTime = true;
      excusedUntil = getTime("Excused until: ", meetingEnd);
      if (excusedUntil.isBefore(meetingStart)) {
        System.out.println(red("Excuse end must not be before meeting start, try again"));
        validTime = false;
      } else if (excusedUntil.isBefore(excusedFrom)) {
        System.out.println(red("Excuse end must not be before excuse start, try again"));
        validTime = false;
      } else if (excusedUntil.isAfter(meetingEnd)) {
        System.out.println(red("Excuse end must not be after meeting end, try again"));
        validTime = false;
      }
    } while (!validTime);
    final Student student = StudentManager.getStudent(id);
    student.setExcusedFrom(excusedFrom);
    student.setExcusedUntil(excusedUntil);
    System.out.printf(green("%s excused from %s until %s%n%n"), student.getName(),
        excusedFrom.format(TIME_FORMATTER), excusedUntil.format(TIME_FORMATTER));
  }

  /**
   * Get user input for name, id, and subteams. Create a new student object with
   * those attributes. Prompt user to log in new student
   */
  private static void addNewStudent() {
    final String name = getNewName("New student name: ");
    final int id = getNewId("New student id: ");
    final Set<Subteam> subteams = getOneOrMoreSubteams("Enter subteam number(s): ");
    StudentManager.initializeNewStudent(name, id, subteams);
    System.out.printf(green("%s succesfully added to spreadsheet%n%n"), name);
  }

  /**
   * Select taget student by id, then set id to user input
   */
  private static void changeStudentId() {
    final int oldId = getUsedId("Student id: ");
    final Student student = StudentManager.getStudent(oldId);
    final String name = student.getName();
    System.out.println("Changing id for " + blue(name));
    final int newId = getNewId("Enter new id: ");
    if (newId == oldId) {
      System.out.println(yellow("No change applied"));
      return;
    }
    System.out.println(yellow("This will change the id of " + name + " from ") +
        blue(Integer.toString(oldId)) + yellow(" to ") +
        blue(Integer.toString(newId)));
    if (getConfirmationDefaultYes("Do you want to proceed?")) {
      student.setId(newId);
      StudentManager.changeId(oldId, newId);
      System.out.printf(green("%s successfully changed id from %d to %d%n%n"),
          name, oldId, newId);
    } else {
      System.out.printf(green("%s id not changed from %d%n%n"), name, oldId);
    }
  }

  /**
   * Select taget student by id, then set name to user input
   */
  private static void changeStudentName() {
    final int id = getUsedId("Student id: ");
    final Student student = StudentManager.getStudent(id);
    final String oldName = student.getName();
    System.out.println("Changing name for " + blue(oldName));
    final String newName = getNewName("Enter new name: ");
    if (newName.equals(oldName)) {
      System.out.println(yellow("No change applied"));
      return;
    }
    System.out.println(yellow("This will change the name of ") +
        blue(oldName) + yellow(" to ") + blue(newName));
    if (getConfirmationDefaultYes("Do you want to proceed?")) {
      student.setName(newName);
      StudentManager.changeName(oldName, newName);
      System.out.printf(green("%s successfully changed name from %s%n%n"),
          newName, oldName);
    } else {
      System.out.printf(green("%s name not changed%n%n"), oldName);
    }
  }

  /**
   * Select taget student by id, then set subteams to user input
   */
  private static void changeStudentSubteams() {
    final int id = getUsedId("Student id: ");
    final Student student = StudentManager.getStudent(id);
    final String name = student.getName();
    final Set<Subteam> oldSubteams = student.getSubteams();
    System.out.printf("Changing subteams for %s from %s%n", name, blue(oldSubteams.toString()));
    final Set<Subteam> newSubteams = getOneOrMoreSubteams("Enter new subteam(s): ");
    if (newSubteams.equals(oldSubteams)) {
      System.out.println(yellow("No change applied"));
      return;
    }
    System.out.println(yellow("This will change the subteams of " + name + " from ")
        + blue(oldSubteams.toString()) + yellow(" to ")+ blue(newSubteams.toString()));
    if (getConfirmationDefaultYes("Do you want to proceed?")) {
      student.setSubteams(newSubteams);
      System.out.printf(green("%s successfully changed subteams from %s to %s%n%n"),
          name, oldSubteams, newSubteams);
    } else {
      System.out.printf(green("%s subteams not changed from %s%n%n"), name, oldSubteams);
    }
  }

  /**
   * Select target student by id, then remove them from the spreadsheets
   */
  private static void removeStudent() {
    final int id = getUsedId("Student id: ");
    final Student student = StudentManager.getStudent(id);
    final String name = student.getName();
    System.out.println(yellow("This will remove ") + blue(name) +
        yellow(" from the spreadsheets and all login data will be lost"));
    if (getConfirmationDefaultNo("Do you want to proceed?")) {
      StudentManager.removeStudent(student);
      System.out.printf(green("%s successfully removed from spreadsheets%n%n"), name);
    } else {
      System.out.printf(green("%s not removed from spreadsheets%n%n"), name);
    }
  }

  /**
   * Reset spreadsheeets for the start of a new season. Remove all login and
   * meeting data but keep student names, ids, and subteams
   */
  private static void endSeason() {
    System.out.println(yellow("This will remove all login data from the spreadsheets"));
    if (getConfirmationDefaultNo("Do you want to proceed?")) {
      LoginInfo.reset();
      MeetingInfo.reset();
      StudentInfo.reset();
      System.out.println(green("Spreadsheets succesfully reset"));
      System.out.println(green("If this was a mistake, restart the program" + 
          " and run \"restoreBackups\""));
    } else {
      System.out.println(green("Spreadsheets not reset"));
    }
  }

  /**
   * Restore all spreadsheets with the data of their backup files. This will
   * overwrite the contents of the main files, so run carefully
   */
  private static void restoreBackups() {
    System.out.println(yellow("This will overwrite spreadsheet data with " + 
        "data from backup files"));
    final boolean confirmed = getConfirmationDefaultNo("Do you want to proceed?");
    if (confirmed) {
      if (LoginInfo.restore() && MeetingInfo.restore() && StudentInfo.restore()) {
        System.out.println(green("Backups successfully restored"));
        System.out.println(green("Please restart the program\n"));
        System.exit(0); // Exit program because everything needs to be reinitialized
      } else {
        System.out.println(red("Failed to restore backups"));
      }
    } else {
      System.out.println(green("Backups not restored\n"));
    }
  }

  /**
   * Get dimensions of each field printed in the printStatus() method.
   * Student name and id have variable width so those are calculated to be the
   * maximum length amongst all students
   * @return Array containing widths of each field
   */
  private static int[] getStatusFieldDimensions() {
    int maxNameLength = 0;
    int maxIdLength = 0;
    for (final Student student : StudentManager.getAllStudents()) {
      final int nameLength = student.getName().length();
      final int id = student.getId();
      final int idLength = (int) Math.ceil(Math.log10(id));
      if (nameLength > maxNameLength) {
        maxNameLength = nameLength;
      }
      if (idLength > maxIdLength) {
        maxIdLength = idLength;
      }
    }
    final int nameLength = Math.max(maxNameLength, MIN_NAME_LENGTH);
    final int idLength = Math.max(maxIdLength, MIN_ID_LENGTH);
    return new int[] {
      nameLength + 1,  // one space of padding on left
      idLength,
      LOGIN_LENGTH,
      LOGOUT_LENGTH,
      ATTENDANCE_LENGTH
    };
  }

  /**
   * Print the header row for the printStatus method. Each column name is
   * formatted with the color blue and aligned to the center.
   * @param fieldDimensions  number of spaces wide each field is
   */
  private static void printStatusHeader(final int[] fieldDimensions) {
    final String[] header = {
      "NAME",
      "ID",
      "LOGIN",
      "LOGOUT",
      "ATTENDANCE"
    };

    for (int i = 0; i < fieldDimensions.length; i++) {
      final int length = fieldDimensions[i];
      final String label = cyan(header[i]);
      System.out.print(alignCenter(label, length + COLOR_LENGTH_OFFSET) + " | ");
    }
    System.out.print("\b\b \n"); // erase extra "| " at end of line
    for (int i = 0; i < fieldDimensions.length; i++) {
      final int length = fieldDimensions[i];
      final String dashes = new String(new char[length]).replace('\0', '-');
      System.out.printf("%s-+-", dashes);
    }
    System.out.print("\b\b  \n"); // erase extra "+-" at end of line
  }


  /**
   * Parses name, id, login & logout times, and attendance percent.
   * Formats with color and alignment.
   * @param student  student to parse information from
   * @param fieldDimensions  number of spaces wide each field is
   * @return Array of formatted student parameters
   */
  private static String[] getStatusStudentInfo(
      final Student student,
      final int[] fieldDimensions
  ) {
    final String name = blue(student.getName());
    final String id = blue(Integer.toString(student.getId()));
    final LocalTime loginTime = student.getLoginTime();
    final String loginStr;
    if (student.hasLoggedIn()) {
      loginStr = green(TIME_FORMATTER.format(loginTime));
    } else {
      loginStr = red("N/A");
    }
    final LocalTime logoutTime = student.getLogoutTime();
    final String logoutStr;
    if (student.hasLoggedOut()) {
      logoutStr = green(TIME_FORMATTER.format(logoutTime));
    } else {
      logoutStr = red("N/A");
    }
    final String percentHoursAttended = Integer.toString((int) Math.round(
        student.percentHoursAttended())) + "%";
    final String attendancePercent;
    if (student.isPassingRequirement()) {
      attendancePercent = green(percentHoursAttended);
    } else {
      attendancePercent = red(percentHoursAttended);
    }
    return new String[] {
        alignRight(name,               COLOR_LENGTH_OFFSET + fieldDimensions[NAME]),
        alignRight(id,                 COLOR_LENGTH_OFFSET + fieldDimensions[ID]),
        alignCenter(loginStr,          COLOR_LENGTH_OFFSET + fieldDimensions[LOGIN]),
        alignCenter(logoutStr,         COLOR_LENGTH_OFFSET + fieldDimensions[LOGOUT]),
        alignCenter(attendancePercent, COLOR_LENGTH_OFFSET + fieldDimensions[ATTENDANCE])
    };
  }

  /**
   * Print name and id of each student, their login and logout times, and
   * percentage of required hours attended
   */
  private static void printStatus() {
    final int[] fieldDimensions = getStatusFieldDimensions();
    printStatusHeader(fieldDimensions);
    for (final Student student : StudentManager.getAllStudentsSorted()) {
      final String[] studentInfo = getStatusStudentInfo(student, fieldDimensions);
      for (int i = 0; i < studentInfo.length; i++) {
        final String data = studentInfo[i];
        System.out.print(data + " | ");
      }
    System.out.print("\b\b \n"); // erase extra "| " at end of line
    }
    System.out.println();
  }

  /**
   * Get user input. If user inputs an id, then log student in or out. If user
   * enters a command, execute that command
   */
  public static void prompt() {
    final String input = getInput("Enter student id or command: ");
    if (Input.isInteger(input)) {
      final int id = Integer.parseInt(input);
      final Student student = StudentManager.getStudent(id);
      if (StudentManager.containsId(id)) {
        if (!student.isLoggedIn()) {
          student.logIn();
        } else {
          student.logOut();
        }
      } else {
        System.out.printf(red("No student with id %d, try again%n"), id);
      }
    } else { // input is command (String)
      Input.commandAction(input);
    }
  }

  /**
   * Precondition: meeting was ended, and now is resuming on the same day.
   * Prompt the user to make sure this is what they want
   */
  public static void promptResumeEndedMeeting() {
    if (getConfirmationDefaultYes("Today's meeting was already ended. " +
        "Would you like to resume it?")) {
      Meeting.resumeEndedMeeting();
    }
  }

  /**
   * Print project logo
   */
  public static void welcome() {
    System.out.println(blue("""
     
              __   ___   _    _____ _______   _______ ____  _ _____________
              \\ \\ / / | | |  /  __ V  _  \\ | | /  _  V  _ \\| |___ ___/  __/
              |  V  | \\_| |  | |  \\| | | | | | | | | | | \\ | |  | |  | |__ 
              | \\ / |\\__  |  | |__ | |_| | | | | | | | |_/ | |  | |  | __/ 
              | |V| |__ | |  |  __/|  _  | \\ / | \\ / |  _ <| |  | |  | \\ __
              | | | /\\ V /   | |   | | | |\\ V / \\ V /\\ | | | |  | |   \\ V /
               \\| |/  \\ /     \\|    \\| |/  \\ /   \\ /  \\| |/|/   |/     \\ / 
                \\ /    V       \\     \\ /    V     V    \\ / /    /       V  
        """) + red("""
        __      _____ ____  _ _   __  __   _________   _______ _____ ____ ____  
        \\ |    /  _  V  _ \\| | \\ | /  \\ \\ / /  _  \\ \\ | /  _  V  _  V  _ V  _ \\ 
        | |    | | | | | |_| |  \\| |  |  V  | | | |  \\| | | | | | |_| | \\| | \\ \\
        | |    | | | | | __| |     |  |     | |_| |     | |_| | | __| |__| |_/ /
        | |   /| | | | | \\ | | |\\  |  | \\ / |  _  | |\\  |  _  | | \\ | __/|  _ | 
        | |  / | \\ / | \\ / | | | \\ |  | |V| | | | | | \\ | | | | \\ / | \\ _| | \\ \\
        | | / / \\ V / \\ V /| | | | |  | | | | | | | | | | | | /\\ V / \\ V | | | |
        | |/ /   \\ /   \\ / |/ \\| |/    \\| |/ \\| |/ \\| |/ \\| |/  \\ /   \\ / \\| | |
        |   /     V     V  /   \\ /      \\ /   \\ /   \\ /   \\ /    V     V   \\ | |
        |  /""") +      blue("      _____________ _______   __   _____ _____ ____ __   __      ") + red("| |\n")
        + red("| / ") + blue("      \\___ ___/  __V  _  \\ \\ / /  / __  V  _  V __ \\\\ | | /   ") + red("   \\ |\n")
        + red("|/  ") + blue("         | |  | |_ | | | |  V  |  |/ / /| | | |/ / /| \\_| |      ") + red(" \\|\n")
        + red("/   ") + blue("         | |  | __/| |_| | \\ / |   _ \\ \\| |/  | / /  \\__  |    ") + red("    \\\n")
        + blue("""
                     | |  | \\ _|  _  | |V| |  | \\ | |  /| |/ //|    | |
                     | |   \\ V | | | | | | |   \\ V / \\ V / \\ V /    | |
                     |/     \\ / \\| |/ \\| |/     \\ /   \\ /   \\ /      \\|
                     /       V   \\ /   \\ /       V     V     V        \\
        """));
    System.out.println(red("                         Quinn Yockey September 2023\n"));
  }

  /**
   * Prompt user for a time the meeting should end. Repeats until a valid time
   * matching the given format is entered
   * @return End time entered by user
   */
  public static LocalTime getMeetingEndTime() {
    final LocalTime defaultEndTime = Meeting.defaultEndTime();
    LocalTime endTime;
    boolean afterMeetingStart;
    do {
      endTime = getTime("Enter meeting end time: ", defaultEndTime);
      afterMeetingStart = endTime.isAfter(Meeting.getStartTime());
      if (!afterMeetingStart) {
        System.out.println(red("Meeting end must be after meeting start, try again\n"));
      }
    } while (!afterMeetingStart);
    System.out.println(green("Meeting will end at " + endTime.format(TIME_FORMATTER) + "\n"));
    return endTime;
  }

  /**
   * Prompt user for the subteams required for the current meeting
   * @return set of subteams entered by user
   */
  public static Set<Subteam> getRequiredSubteams() {
    final Set<Subteam> requiredSubteams = 
        getSubteamsDefaultNone("Enter required subteams for this meeting: ");
    System.out.print(green("Meeting required for "));
    if (requiredSubteams.size() == Subteam.values().length) {
      System.out.println(green("all subteams"));
    } else if (requiredSubteams.isEmpty()) {
      System.out.println(green("no subteams"));
    } else {
      for (Subteam team : requiredSubteams) {
        System.out.print(blue(team.toString()) + green(", "));
      }
      System.out.println("\b\b ");
    }
    return requiredSubteams;
  }

}
