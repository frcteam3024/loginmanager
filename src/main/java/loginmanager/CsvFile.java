package loginmanager;

import static loginmanager.Constants.Csv.BACKUP_SUFFIX;
import static loginmanager.Constants.Csv.CSV_DIR;
import static loginmanager.Constants.Csv.CSV_EXTENSION;
import static loginmanager.Constants.Csv.DELIMETER;
import static loginmanager.Constants.Csv.HEADER_ROW;
import static loginmanager.Constants.Csv.NUM_DECIMAL_PLACES;
import static loginmanager.Constants.Time.DATE_FORMATTER;
import static loginmanager.Constants.Time.TIME_FORMATTER;
import static loginmanager.TextFormat.red;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import loginmanager.Constants.Subteam;

/**
 * Outline class containing basic methods to edit CSV files
 */
public class CsvFile {

  private final File file;
  private final File backup;
  private List<List<String>> fileContents;

  /**
   * Initialize a new Csv file object with name filename. If the file doesn't
   * already exist, create it, and then set the first row to defaultHeader
   * @param fileName
   */
  protected CsvFile(final String fileName, final List<String> defaultHeader) {
    final String backupName = fileName + BACKUP_SUFFIX;
    file = new File(CSV_DIR, fileName + CSV_EXTENSION);
    backup = new File(CSV_DIR, backupName + CSV_EXTENSION);
    if (!CSV_DIR.exists()) {
      CSV_DIR.mkdir();
    }
    if (!file.exists()) {
      fileContents = new ArrayList<>();
      fileContents.add(defaultHeader);
    } else {
      fileContents = readFileContents(file);
    }
  }

  /**
   * Get a 2D ArrayList containing the data of the given file
   * @param target  which file to read: main or backup
   * @return 2D ArrayList of spreadsheet contents
   */
  private List<List<String>> readFileContents(final File target) {
    final List<List<String>> contents = new ArrayList<>();
    try (final BufferedReader reader = new BufferedReader(new FileReader(target))) {
      String line;
      while ((line = reader.readLine()) != null) {
        contents.add(new ArrayList<>(Arrays.asList(line.split(DELIMETER))));
      }
    } catch (IOException e) {
      System.out.printf(red("Error: file not found \"%s\"%n"),
          target.getAbsolutePath());
    }
    return contents;
  }

  /**
   * Write fileContents data to selected file
   * @param target  which file to save: main or backup
   */
  private void writeFileContents(final File target) {
    try (final BufferedWriter writer = new BufferedWriter(new FileWriter(target))) {
      for (final List<String> row : fileContents) {
        final String line = String.join(DELIMETER, row);
        writer.append(line);
        writer.newLine();
      }
    } catch (IOException e) {
      System.out.printf(red("Error: file not found \"%s\"%n"),
          target.getAbsolutePath());
    }
  }

  /**
   * Write fileContents data to main file
   */
  private void writeFile() {
    writeFileContents(file);
  }

  /**
   * Write fileContents data to backup file
   */
  protected void writeBackup() {
    writeFileContents(backup);
  }

  /**
   * Overwrite file contents with contents of backup
   * @return True if backup restored, false if not
   */
  protected boolean restoreBackup() {
    if (backup.exists()) {
      fileContents = readFileContents(backup);
      writeFile();
      return true;
    } else {
      System.out.printf(red("No backup file found at %s%n"),
          backup.getAbsolutePath());
      return false;
    }
  }

  /**
   * Get the cell contents of main file at the given row and column as a String
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @return Cell contents as String
   */
  protected String getCellContentsStr(final int row, final int col) {
    return fileContents.get(row).get(col);
  }

  /**
   * Get the cell contents of main file at the given row and column as an int
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @return Cell contents as int
   */
  protected int getCellContentsInt(final int row, final int col) {
    return Integer.parseInt(getCellContentsStr(row, col));
  }

  /**
   * Get the cell contents of main file at the given row and column as a double
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @return Cell contents as double
   */
  protected double getCellContentsDouble(final int row, final int col) {
    return Double.parseDouble(getCellContentsStr(row, col));
  }

  /**
   * Get the cell contents of main file at the given row and column as a boolean
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @return Cell contents as boolean
   */
  protected boolean getCellContentsBool(final int row, final int col) {
    return Boolean.parseBoolean(getCellContentsStr(row, col));
  }

  /**
   * Get the cell contents of main file at the given row and column as a
   * LocalTime object
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @return Cell contents as LocalTime
   * @apiNote A cell containing "null" will return null
   * @apiNote Contents not formatted as defined in Constants.Time.TIME_FORMATTER
   * will throw a parsing exception.
   */
  protected LocalTime getCellContentsTime(final int row, final int col) {
    final String timeStr = getCellContentsStr(row, col);
    if (timeStr.equals("null")) {
      return null;
    }
    return LocalTime.parse(timeStr, TIME_FORMATTER);
  }

  /**
   * Get the cell contents of main file at the given row and column as a
   * LocalDate object
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @return Cell contents as LocalDate
   * @apiNote A cell containing "null" will return null
   * @apiNote Contents not formatted as defined in Constants.Time.DATE_FORMATTER
   * will throw a parsing exception.
   */
  protected LocalDate getCellContentsDate(final int row, final int col) {
    final String dateStr = getCellContentsStr(row, col);
    if (dateStr.equals("null")) {
      return null;
    }
    return LocalDate.parse(dateStr, DATE_FORMATTER);
  }

  /**
   * Get the cell contents of main file at the given row and column as a Set of
   * Subteams
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @return Cell contents as Set of Subteams
   * @apiNote Content not formatted properly will throw a parsing exception.
   */
  protected Set<Subteam> getCellContentsSubteams(final int row, final int col) {
    final Set<Subteam> subteams = new LinkedHashSet<>();
    final String str = getCellContentsStr(row, col);
    if (str.equals("null")) {
      return subteams;
    }
    /* "\\s*,\\s*" regex below means any number of spaces followed by a comma 
     * followed by any number of spaces */
    final String[] tokens = str.split("\\s*,\\s*");
    for (final String token : tokens) {
      subteams.add(Subteam.valueOf(token));
    }
    return subteams;
  }

  /**
   * Set the cell contents of main file at the given row and column to provided
   * String data
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @param newData  String value to write
   */
  protected void setCellContents(
      final int row,
      final int col,
      final String newData
  ) {
    final List<String> studentInfo = fileContents.get(row);
    studentInfo.set(col, newData);
    fileContents.set(row, studentInfo);
    writeFile();
  }

  /**
   * Set the cell contents of main file at the given row and column to provided
   * int data
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @param newData  int value to write
   */
  protected void setCellContents(
      final int row,
      final int col,
      final int newData
  ) {
    setCellContents(row, col, Integer.toString(newData));
  }

  /**
   * Set the cell contents of main file at the given row and column to provided
   * double data
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @param newData  double value to write
   */
  protected void setCellContents(
      final int row,
      final int col,
      final double newData
  ) {
    setCellContents(row, col, Double.toString(round(newData)));
  }

  /**
   * Set the cell contents of main file at the given row and column to provided
   * boolean data
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @param newData  boolean value to write
   */
  protected void setCellContents(
      final int row,
      final int col,
      final boolean newData
  ) {
    setCellContents(row, col, Boolean.toString(newData));
  }

  /**
   * Set the cell contents of main file at the given row and column to provided
   * LocalTime data
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @param newData  LocalTime value to write
   * @apiNote will write "null" if newData is null
   */
  protected void setCellContents(
      final int row,
      final int col,
      final LocalTime newData
  ) {
    if (newData == null) {
      setCellContents(row, col, "null");
    } else {
      setCellContents(row, col, newData.format(TIME_FORMATTER));
    }
  }

  /**
   * Set the cell contents of main file at the given row and column to provided
   * LocalDate data
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @param newData  LocalDate value to write
   * @apiNote will write "null" if newData is null
   */
  protected void setCellContents(
      final int row,
      final int col,
      final LocalDate newData
  ) {
    if (newData == null) {
      setCellContents(row, col, "null");
    } else {
      setCellContents(row, col, newData.format(DATE_FORMATTER));
    }
  }

  /**
   * Set the cell contents of main file at the given row and column to provided
   * Subteam Set data
   * @param row  spreadsheet row
   * @param col  spreadsheet column
   * @param newData  Subteam Set value to write
   * @apiNote will write "null" if newData is null
   */
  protected void setCellContents(
      final int row,
      final int col,
      final Set<Subteam> newData
  ) {
    String str;
    if (newData.equals(Collections.emptySet())) {
      str = "null";
    } else {
      str = newData.toString();
      // remove brackets
      str = str.replace("[", "").replace("]", "");
    }
    setCellContents(row, col, str);
  }

  /**
   * Append provided string data to the end of the given row of the file
   * @param row  spreadsheet row
   * @param data  String data to append
   */
  protected void appendToRow(final int row, final String data) {
    final List<String> rowData = fileContents.get(row);
    rowData.add(data);
    writeFile();
  }

  /**
   * Append provided int data to the end of the given row of the file
   * @param row  spreadsheet row
   * @param data  double data to append
   */
  protected void appendToRow(final int row, final double data) {
    appendToRow(row, Double.toString(data));
  }

  /**
   * Append provided double data to the end of the given row of the file
   * @param row  spreadsheet row
   * @param data  boolean data to append
   */
  protected void appendToRow(final int row, final boolean data) {
    appendToRow(row, Boolean.toString(data));
  }

  /**
   * Append provided LocalTime data to the end of the given row of the file
   * @param row  spreadsheet row
   * @param data  LocalTime data to append
   */
  protected void appendToRow(final int row, final LocalTime data) {
    appendToRow(row, data.format(TIME_FORMATTER));
  }

  /**
   * Append provided LocalDate data to the end of the given row of the file
   * @param row  spreadsheet row
   * @param data  LocalDate data to append
   */
  protected void appendToRow(final int row, final LocalDate data) {
    appendToRow(row, data.format(DATE_FORMATTER));
  }

  /**
   * Append provided subteam set data to the end of the given row of the file
   * @param row  spreadsheet row
   * @param data  Subteam Set data to append
   */
  protected void appendToRow(final int row, final Set<Subteam> data) {
    appendToRow(row, data.toString().toLowerCase());
  }

  /**
   * @return Number of rows in file
   */
  protected int getNumRows() {
    return fileContents.size();
  }

  /**
   * @return Number of columns in file
   */
  protected int getNumCols() {
    // should be the same for all rows, use header for convenience
    return fileContents.get(HEADER_ROW).size();
  }

  /**
   * @return Index of the first empty row at the end of the file
   */
  protected int getNextAvailableRow() {
    return getNumRows();
  }

  /**
   * Create a new empty row and append it to the bottom of the file
   */
  protected void newRow() {
    final List<String> emptyRow = new ArrayList<>();
    for (int i = 0; i < getNumCols(); i++) {
      emptyRow.add(null);
    }
    fileContents.add(emptyRow);
    writeFile();
  }

  /**
   * Remove the row at the given index from the file
   * @param row  spreadsheet row to remove
   */
  protected void removeRow(final int row) {
    fileContents.remove(row);
    writeFile();
  }

  /**
   * Remove the column at the given index from the file
   * @param col  spreadsheet column to remove
   */
  protected void removeCol(final int col) {
    for (int row = getNumRows() - 1; row >= 0; row--) {
      final List<String> rowData = fileContents.get(row);
      rowData.remove(col);
      fileContents.set(row, rowData);
    }
    writeFile();
  }

  /**
   * Round given number to number of decimal places specified in
   * Contsants.Csv.NUM_DECIMAL_PLACES
   * @param number  number to round
   * @return rounded number
   */
  private double round(final double number) {
    final double scalar = Math.pow(10, NUM_DECIMAL_PLACES);
    return Math.round(number * scalar) / scalar; 
  }

}
