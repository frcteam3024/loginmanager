package loginmanager;

import static loginmanager.Constants.Time.HOURS_PER_DAY;
import static loginmanager.Constants.Time.MINS_PER_HOUR;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Time {
  private Time() {}

  /**
   * @param start
   * @param end
   * @return number of hours between start and end
   */
  public static double hoursBetween(final LocalTime start, final LocalTime end) {
    // ChronoUnit.between returns integer so get value for minutes then convert
    final double difference = ChronoUnit.MINUTES.between(start, end) / MINS_PER_HOUR;
    /* if we need to pull late night meetings, taking difference modulo 24
     * means end times after midnight won't return a negative result */
    return difference % HOURS_PER_DAY;
  }

  /**
   * @param start
   * @param end
   * @return Number of hours between start and end that have passed. If current
   * time is before start, return 0. If current time is between start and end, 
   * return hours between start and now. If current time is after end, return
   * hours between start and end.
   */
  public static double hoursElapsedBetween(
      final LocalTime start,
      LocalTime end
  ) {
    final LocalTime now = LocalTime.now();
    if (now.isBefore(start)) {
      return 0;
    }
    // now is after start:
    if (now.isBefore(end)) {
      end = now;
    }
    return Time.hoursBetween(start, end);
  }

  /**
   * @param date
   * @return true if date is a weekend, false if it's a weekday
   */
  public static boolean isWeekend(final LocalDate date) {
    final DayOfWeek dayOfWeek = date.getDayOfWeek();
    return dayOfWeek.equals(DayOfWeek.SATURDAY) || dayOfWeek.equals(DayOfWeek.SUNDAY);
  }
}
