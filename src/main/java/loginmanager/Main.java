package loginmanager;

import static loginmanager.Constants.MeetingStatus.DELETED;
import static loginmanager.Constants.MeetingStatus.ENDED;
import static loginmanager.Constants.MeetingStatus.OVERTIME;

import java.time.LocalTime;
import java.util.Set;

import loginmanager.Constants.Subteam;

public class Main {

  /**
   * @param args  command line arguments -- not used
   * Start or resume meeting, then prompting the user for input until the
   * meeting is ended
   */
  public static void main(String[] args) {
    Input.welcome();
    switch (Meeting.getStatus()) {
      case NOT_STARTED:
      case DELETED:
        LocalTime meetingEndTime = Input.getMeetingEndTime();
        Set<Subteam> requiredSubteams = Input.getRequiredSubteams();
        Meeting.newMeeting(meetingEndTime, requiredSubteams);
        break;
      case IN_SESSION:
      case OVERTIME:
        Meeting.resume();
        break;
      case ENDED:
        Input.promptResumeEndedMeeting();
        break;
    }
    while (!Meeting.getStatus().equals(ENDED)) {
      Input.prompt();
      if (Meeting.getStatus().equals(DELETED)) {
        break;
      } else if (Meeting.isOver() && !Meeting.getStatus().equals(OVERTIME) &&
          !Meeting.getStatus().equals(ENDED)) {
        Meeting.toOvertime();
      }
    }
  }
}
