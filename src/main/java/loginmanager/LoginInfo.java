package loginmanager;

import static loginmanager.Constants.LoginCsv.DATE_BLOCK_WIDTH;
import static loginmanager.Constants.LoginCsv.DATE_START_INDEX;
import static loginmanager.Constants.LoginCsv.DEFAULT_HEADER;
import static loginmanager.Constants.LoginCsv.HEADER_ROW;
import static loginmanager.Constants.LoginCsv.HEADER_WIDTH;
import static loginmanager.Constants.LoginCsv.HOURS_ATTENDED_OFFSET;
import static loginmanager.Constants.LoginCsv.LOGIN_COL_OFFSET;
import static loginmanager.Constants.LoginCsv.LOGOUT_COL_OFFSET;
import static loginmanager.Constants.LoginCsv.NAME;
import static loginmanager.Constants.Time.DATE_FORMATTER;
import static loginmanager.TextFormat.red;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class LoginInfo extends CsvFile {

  /** index of the start column of the block of the current day's info fields */
  private static int currentDateCol;
  /** map linking student objects to their corresponding row numbers in the file */
  private static final Map<Student, Integer> studentToRow = new LinkedHashMap<>();
  /** instance of LoginInfo class to call CSVFile methods in a static context */
  private static LoginInfo instance;

  /**
   * Initialize LoginInfo as an instance of CsvFile with the name "login-sheet"
   */
  private LoginInfo() {
    super("login-info", new ArrayList<>(DEFAULT_HEADER));
  }

  /**
   * Scan all rows of the file and add a map link between each student and 
   * row number
   */
  static {
    for (int row = HEADER_WIDTH; row < getInstance().getNumRows(); row ++) {
      final String name = getInstance().getCellContentsStr(row, NAME);
      if (StudentManager.containsName(name)) {
        final Student student = StudentManager.getStudent(name);
        studentToRow.put(student, row);
      } else {
        System.out.printf(red("Student %s doesn't exist in student info sheet%n"), name);
      }
    }
  }

  /**
   * Get an instance of the LoginInfo class to use when calling any non-static
   * methods of CSVFile superclass in a static context. Only one instance will
   * ever be created.
   * @return an instance of the LoginInfo class
   */
  private static synchronized LoginInfo getInstance() {
    if (instance == null) {
      instance = new LoginInfo();
    }
    return instance;
  }

  /**
   * Set name of student to newName in file
   * @param student  student to set name of
   * @param newName  new name of student
   */
  public static void setName(
      final Student student,
      final String newName
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, NAME, newName);
  }

  /** 
   * Get login time of student for current meeting, or null if they haven't
   * logged in
   * @param student  student to get login time of
   * @return student login time
   */
  public static LocalTime getLoginTime(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsTime(row, currentDateCol + LOGIN_COL_OFFSET);
  }

  /**
   * Set login time of student in file
   * @param student  student to set login time of
   * @param loginTime  when studet logged in
   */
  public static void setLoginTime(
      final Student student,
      final LocalTime loginTime
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, currentDateCol + LOGIN_COL_OFFSET, loginTime);
  }

  /**
   * Get logout time of student for current meeting, or null if they haven't
   * logged out
   * @param student  student to get logout time of
   * @return student logout time
   */
  public static LocalTime getLogoutTime(final Student student) {
    final int row = studentToRow.get(student);
    return getInstance().getCellContentsTime(row, currentDateCol + LOGOUT_COL_OFFSET);
  }

  /**
   * Set logout time of student in file
   * @param student  student to set logout time of
   * @param logoutTime  when studet logged out
   */
  public static void setLogoutTime(
      final Student student,
      final LocalTime logoutTime
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, currentDateCol + LOGOUT_COL_OFFSET, logoutTime);
  }

  /**
   * Set number of hours student attended the current meeting in file
   * @param student  student to set hours attend of
   * @param meetingHoursAttended  number of hours student attended current meeting
   */
  public static void setMeetingHoursAttended(
      final Student student,
      final double meetingHoursAttended
  ) {
    final int row = studentToRow.get(student);
    getInstance().setCellContents(row, currentDateCol + HOURS_ATTENDED_OFFSET, meetingHoursAttended);
  }

  /**
   * Create a new row in file containing newStudent. All previous meetings will
   * have null for login and logout times and hours attended will be 0.
   * @param newStudent  student to add
   */
  public static void addNewStudent(final Student newStudent) {
    final String name = newStudent.getName();
    final int row = getInstance().getNextAvailableRow();
    studentToRow.put(newStudent, row);
    getInstance().newRow();
    getInstance().setCellContents(row, NAME, name);
    for (int col = DATE_START_INDEX; col < getInstance().getNumCols(); col += DATE_BLOCK_WIDTH) {
      getInstance().setCellContents(row, col + LOGIN_COL_OFFSET, "null");
      getInstance().setCellContents(row, col + LOGOUT_COL_OFFSET, "null");
      getInstance().setCellContents(row, col + HOURS_ATTENDED_OFFSET, 0);
    }
  }

  /**
   * Remove the spreadsheet row containing student and remove the mapping
   * between student and their row
   * @param student  student to remove
   */
  public static void removeStudent(final Student student) {
    final int studentRow = studentToRow.get(student);
    final Student[] allStudents = studentToRow.keySet().toArray(new Student[0]);
    // move each student up one row
    for (int i = studentRow + 1 - HEADER_WIDTH; i < allStudents.length; i++) {
      studentToRow.replace(allStudents[i], i - 1 + HEADER_WIDTH);
    }
    getInstance().removeRow(studentRow);
    studentToRow.remove(student);
  }

  /**
   * Create new columns for the current date with login, lougout, and hours
   * attended fields, initialized to null, null, and 0, respectively
   * @param date  date to create meeting entries for
   */
  public static void newMeeting(final LocalDate date) {
    final String dateStr = date.format(DATE_FORMATTER);
    currentDateCol = getInstance().getNumCols();
    getInstance().appendToRow(HEADER_ROW, dateStr + "_LOGIN");
    getInstance().appendToRow(HEADER_ROW, dateStr + "_LOGOUT");
    getInstance().appendToRow(HEADER_ROW, dateStr + "_HOURS_ATTENDED");
    for (int row = HEADER_WIDTH; row < getInstance().getNumRows(); row++) {
      getInstance().appendToRow(row, "null");
      getInstance().appendToRow(row, "null");
      getInstance().appendToRow(row, 0);
    }
  }

  /**
   * Tasks to run when meeting is resumed:
   * Reinitialize currentDateCol
   */
  public static void resumeMeeting() {
    currentDateCol = getInstance().getNumCols() - DATE_BLOCK_WIDTH;
  }

  /**
   * Tasks to run when meeting is ended:
   * Backup the file
   */
  public static void endMeeting() {
    getInstance().writeBackup();
  }

  /**
   * Delete all login data from spreadsheet, but still keep student names
   */
  public static void reset() {
    final int lastCol = getInstance().getNumCols() - 1;
    for (int col = lastCol; col >= DATE_START_INDEX; col--) {
      getInstance().removeCol(col);
    }
  }

  /**
   * Overwrite file contents with contents of backup
   * @return True if backup restored, false if not
   */
  public static boolean restore() {
    return getInstance().restoreBackup();
  }

  /**
   * @return String containing file contents, cells separated by tabs
   */
  public static String toStr() {
    return getInstance().toString();
  }

}
