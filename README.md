# My Favorite Login Manager

My Favorite Login Manager is a program developed by FRC Team #3024 to automate the tedious process of logging members in with a pad of paper. This project was designed to allow students to log in faster and to relieve the burden placed on the admin team of manually copying data from the paper over to a digital spreadsheet. By making the whole process digital, this should make it much easier to calculate which students are passing the attendance requirement and free up admin team members to do more important things.

## Installation

This project is designed to be run on a raspberry pi through the command line. To get started, install java and git, then clone this repo.

```bash
sudo apt update
sudo apt install git default-jdk default-jre
git clone git@gitlab.com:frcteam3024/loginmanager.git
```

## Running

A run script is provided for convenience, which syncs the local repo with the remote repo, compiles the program, then runs it. After the program is exited by the user, the repos are synced again, and finally any changes to the spreadsheets are pushed. This can be achieved by moving into the project directory and then running the script.

```bash
cd path/to/loginmanager
./run
```

## Usage

Upon starting the login manager, the user will be greeted with the project logo (that I will not disclose how much time I spent designing). They will then be prompted for the meeting's end time and which subteams are required to attend. Enter end time formatted like `11:30am` or `4:00pm`. Not entering anything will select the default end time for the current day. To select subteams, type any combination of numbers to select their corresponding subteams, or leave the field empty to select all subteams. For example, to start a meeting just for electrical and programming, type `24`.

Once a meeting has been started, there are many commands the user can run:

### Student ID

Entering a student ID number once will log a student in, and entering it a second time will log the student out.

### `help`

Prints a list of all available commands and a short description for each. This list will also be printed when the user enters an invalid command.

### `changeName`

Prompts the user from the id of the student to change the name of, and then for their new name.

### `changeSubteam`

Prompts the user from the id of the student to change the subteam of, and then for their new subteam. As with entering subteams for a meeting, enter the number corresponding to the desired subteam, or enter multiple numbers if this student will be part of multiple subteams.

### `endMeeting`

RUN THIS COMMAND AT THE END OF EVERY MEETING. I know that is probably prone to error, but it's the only way I could come up with that also allowed tracking of overtime hours. Just find some way to make it happen. Any students logged in get logged out, and the hours required for students that didn't show up get updated.

### `endSeason`

This command will wipe all login data stored in the spreadsheets. All students will remain and they will keep their ids and subteams, but this erases login and logout times, the number of hours they have attended, hours required, percentage of required hours attended, and if they are passing the attendance requirement.

### `excuse`

Prompts the user for the id of the student to excuse and the times the student is excused from and until. Any hours attended within this excused timeframe will count towards the number of hours they have attended but not towards the number of hours required for them.

### `newStudent`

Prompt user for name of new student, their id number, and subteam(s) to join. Add the new student to the spreadsheets.

### `removeStudent`

Prompt the user for the id of the student to remove, and then remove that student from the spreadsheets after confirmation

### `restoreBackups`

Copies all data from the backup spreadsheets to the main spreadsheets. This will override all data in the main spreadsheets.

### `status`

Print a table listing all students with their name, id, login time, logout time, percent of required hours attended, and is they are passing the attendance requirement.

## Future Development

I'm going off to college in the fall so I likely won't be actively maintaining this project, that task will fall on future programming team members. I have tried to provide as much documentation as possible to make it easier to extend this app's functionality. If you have any questions about the code of this project, send me an email at qyockey@tutanota.de or ping me on discord @qyockey. Although in a couple years the details probably won't be as fresh in my head, I'd still love to help out!

## License

[WTFPL](https://choosealicense.com/licenses/wtfpl/)
